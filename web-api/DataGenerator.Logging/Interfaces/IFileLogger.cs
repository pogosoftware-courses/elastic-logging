using DataGenerator.Logging.Models;

namespace DataGenerator.Logging.Interfaces
{
    public interface IFileLogger
    {
        void Error(LogDetail errorLogDetail);
    }
}