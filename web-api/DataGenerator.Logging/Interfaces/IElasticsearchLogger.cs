using DataGenerator.Logging.Models;

namespace DataGenerator.Logging.Interfaces
{
    public interface IElasticsearchLogger
    {
        void Information(LogDetail logDetail);
    }
}