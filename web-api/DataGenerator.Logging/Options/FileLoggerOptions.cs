namespace DataGenerator.Logging.Options
{
    public class FileLoggerOptions
    {
        public string Path { get; set; }

        public string FileName { get; set; }
    }
}