namespace DataGenerator.Logging.Options
{
    public class ElasticsearchLoggerOptions
    {
        public string Url { get; set; }

        public bool InlineFields { get; set; }

        public string IndexFormat { get; set; }
    }
}