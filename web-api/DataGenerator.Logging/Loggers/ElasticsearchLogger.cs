using System;
using DataGenerator.Logging.Interfaces;
using DataGenerator.Logging.Models;
using DataGenerator.Logging.Options;
using Microsoft.Extensions.Options;
using Serilog;
using Serilog.Events;

namespace DataGenerator.Logging.Loggers
{
    public class ElasticsearchLogger : IElasticsearchLogger
    {
        private readonly ILogger _logger;

        public ElasticsearchLogger(IOptions<ElasticsearchLoggerOptions> optionAccesor)
        {
            if (optionAccesor == null) throw new ArgumentNullException(nameof(optionAccesor));

            var options = optionAccesor.Value;
            _logger = CreateLogger(options);
        }

        private ILogger CreateLogger(ElasticsearchLoggerOptions options)
        {
            return new LoggerConfiguration()
                .WriteTo.Elasticsearch(options.Url,
                    indexFormat: options.IndexFormat,
                    inlineFields: options.InlineFields)
                .CreateLogger(); 
        }

        public void Information(LogDetail logDetail)
        {
            _logger.Write(LogEventLevel.Information, "{@LogDetail}", logDetail);
        }
    }
}