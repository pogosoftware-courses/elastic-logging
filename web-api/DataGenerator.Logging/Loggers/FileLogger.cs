using System;
using DataGenerator.Logging.Interfaces;
using DataGenerator.Logging.Models;
using DataGenerator.Logging.Options;
using Microsoft.Extensions.Options;
using Serilog;
using Serilog.Events;

namespace DataGenerator.Logging.Loggers
{
    public class FileLogger : IFileLogger
    {
        private readonly ILogger _logger;

        public FileLogger(IOptions<FileLoggerOptions> optionAccesor)
        {
            if (optionAccesor == null) throw new ArgumentNullException(nameof(optionAccesor));

            var options = optionAccesor.Value;
            _logger = CreateLogger(options);
        }

        private ILogger CreateLogger(FileLoggerOptions options)
        {
            var fileName = String.Format(options.FileName, DateTime.Now.ToString("yyyyMMdd"));
            var fileNameWithPath = string.Concat(options.Path, fileName);

            return new LoggerConfiguration()
                .WriteTo.File(fileNameWithPath)
                .CreateLogger();
        }

        public void Error(LogDetail errorLogDetail)
        {
            _logger.Write(LogEventLevel.Error, "{@LogDetail}", errorLogDetail);
        }
    }
}