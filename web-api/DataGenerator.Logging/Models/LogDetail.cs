using System;

namespace DataGenerator.Logging.Models
{
    public class LogDetail
    {
        private LogDetail()
        {
            Timestamp = DateTime.Now;
        }

        private LogDetail(string path, string method, int statusCode, string responseBody)
            : this()
        {
            Path = path;
            Method = method;
            StatusCode = statusCode;
            ResponseBody = responseBody;
        }

        public static LogDetail Create(string path, string method, int statusCode, string responseBody) =>
            new LogDetail(path, method, statusCode, responseBody);

        public DateTime Timestamp { get; private set; }

        public string Path { get; private set; }

        public string Method { get; private set; }

        public int StatusCode { get; private set; }

        public string ResponseBody { get; private set; }
    }
}