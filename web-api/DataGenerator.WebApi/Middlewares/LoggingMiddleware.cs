using System.IO;
using System.Threading.Tasks;
using DataGenerator.Logging.Interfaces;
using DataGenerator.Logging.Models;
using Microsoft.AspNetCore.Http;

namespace DataGenerator.WebApi.Middlewares
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IFileLogger _fileLogger;
        private readonly IElasticsearchLogger _elasticsearchLogger;

        public LoggingMiddleware(RequestDelegate next,
            IFileLogger fileLogger,
            IElasticsearchLogger elasticsearchLogger)
        {
            _next = next;
            _fileLogger = fileLogger;
            _elasticsearchLogger = elasticsearchLogger;
        }

        public async Task Invoke(HttpContext context)
        {
            var originaRequest = context.Request;
            var bodyStream = context.Response.Body;
            var responseBodyStream = new MemoryStream();
            context.Response.Body = responseBodyStream;

            await _next(context);

            var path = originaRequest.Path;
            var method = originaRequest.Method;
            var statusCode = context.Response.StatusCode;

            responseBodyStream.Seek(0, SeekOrigin.Begin);
            var responseBody = new StreamReader(responseBodyStream).ReadToEnd();

            if (context.Response.StatusCode != 200)
            {
                _fileLogger.Error(LogDetail.Create(path, method, statusCode, responseBody));
            }
            else
            {
                _elasticsearchLogger.Information(LogDetail.Create(path, method, statusCode, responseBody));
            }

            responseBodyStream.Seek(0, SeekOrigin.Begin);
            await responseBodyStream.CopyToAsync(bodyStream);
        }
    }
}