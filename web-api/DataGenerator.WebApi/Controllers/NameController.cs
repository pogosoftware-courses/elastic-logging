using System;
using DataGenerator.WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DataGenerator.WebApi.Controllers
{
    [Route("api/name")]
    public class NameController : Controller
    {
        private readonly INameService _nameService;

        public NameController(INameService nameService)
        {
            if (nameService == null) throw new ArgumentNullException(nameof(nameService));

            _nameService = nameService;
        }

        /// <summary>
        /// Get a first name.
        /// </summary>
        /// <returns> First name. </returns>
        [HttpGet("firstname")]
        public IActionResult GetFirstName() =>
            Ok(_nameService.GetFirstName());

        /// <summary>
        /// Get a last name.
        /// </summary>
        /// <returns> Last name. </returns>
        [HttpGet("lastname")]
        public IActionResult GetLastName() =>
            Ok(_nameService.GetLastName());

        /// <summary>
        /// Get a full name.
        /// </summary>
        /// <returns> Full name.</returns>
        [HttpGet("fullname")]
        public IActionResult GetFullName() =>
            Ok(_nameService.GetFullName());

        /// <summary>
        /// Get a random prefix for a name.
        /// </summary>
        /// <returns> Prefix. </returns>
        [HttpGet("prefix")]
        public IActionResult GetPrefix() =>
            Ok(_nameService.GetPrefix());

        /// <summary>
        /// Get a random suffix for a name.
        /// </summary>
        /// <returns> Suffix. </returns>
        [HttpGet("suffix")]
        public IActionResult GetSuffix() =>
            Ok(_nameService.GetSuffix());

        /// <summary>
        /// Get a random job title.
        /// </summary>
        /// <returns> Job title. </returns>
        [HttpGet("jobtitle")]
        public IActionResult GetJobTitle() =>
            Ok(_nameService.GetJobTitle());

        /// <summary>
        /// Get a job description.
        /// </summary>
        /// <returns> Job description.</returns>
        [HttpGet("jobdescriptor")]
        public IActionResult GetJobDescriptor() =>
            Ok(_nameService.GetJobDescriptor());

        /// <summary>
        /// Get a job area expertise.
        /// </summary>
        /// <returns> Job area. </returns>
        [HttpGet("jobarea")]
        public IActionResult GetJobArea() =>
            Ok(_nameService.GetJobArea());

        /// <summary>
        /// Get a type of job.
        /// </summary>
        /// <returns> Job type. </returns>
        [HttpGet("jobtype")]
        public IActionResult GetJobType() =>
            Ok(_nameService.GetJobType());
    }
}