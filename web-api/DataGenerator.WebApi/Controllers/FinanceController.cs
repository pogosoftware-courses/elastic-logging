using System;
using DataGenerator.WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DataGenerator.WebApi.Controllers
{
    [Route("api/finance")]
    public class FinanceController : Controller
    {
        private readonly IFinanceService _financeService;
        public FinanceController(IFinanceService financeService)
        {
            if (financeService == null) throw new ArgumentNullException(nameof(financeService));

            _financeService = financeService;
        }

        /// <summary>
        /// Get an account number. Default length is 8 digits.
        /// </summary>
        /// <returns> Acount number. </returns>
        [HttpGet("account")]
        public IActionResult GetAccount() =>
            Ok(_financeService.GetAccount());

        /// <summary>
        /// Get an account name. Like "savings", "checking", "Home Loan" etc..
        /// </summary>
        /// <returns> Account name. </returns>
        [HttpGet("accountname")]
        public IActionResult GetAccountName() =>
            Ok(_financeService.GetAccountName());

        /// <summary>
        /// Get a random amount. Default 0 - 1000.
        /// </summary>
        /// <returns> Amount. </returns>
        [HttpGet("amount")]
        public IActionResult GetAmount() =>
            Ok(_financeService.GetAmount());

        /// <summary>
        /// Get a transaction type: "deposit", "withdrawal", "payment", or "invoice".
        /// </summary>
        /// <returns> Transaction type. </returns>
        [HttpGet("transactiontype")]
        public IActionResult GetTransactionType() =>
            Ok(_financeService.GetTransactionType());

        /// <summary>
        /// Get a random currency.
        /// </summary>
        /// <returns> Currency. </returns>
        [HttpGet("currency")]
        public IActionResult GetCurrency() =>
            Ok(_financeService.GetCurrency());

        /// <summary>
        /// Get a random credit card number with valid Luhn checksum.
        /// </summary>
        /// <returns> Credit card number. </returns>
        [HttpGet("creditcardnumber")]
        public IActionResult GetCreditCardNumber() =>
            Ok(_financeService.GetCreditCardNumber());

        /// <summary>
        /// Get a credit card CVV.
        /// </summary>
        /// <returns> Credit card CVV. </returns>
        [HttpGet("creditcardcvv")]
        public IActionResult GetCreditCardCvv() =>
            Ok(_financeService.GetCreditCardCvv());

        /// <summary>
        /// Get a random bitcoin address.
        /// </summary>
        /// <returns> Bitcoin address. </returns>
        [HttpGet("bitcoinaddress")]
        public IActionResult GetBitcoinAddress() =>
            Ok(_financeService.GetBitcoinAddress());

        /// <summary>
        /// Get a random ethereum address.
        /// </summary>
        /// <returns> Ethereum address. </returns>
        [HttpGet("ethereumaddress")]
        public IActionResult GetEthereumAddress() =>
            Ok(_financeService.GetEthereumAddress());

        /// <summary>
        /// Get an ABA routing number with valid check digit.
        /// </summary>
        /// <returns> Routing number. </returns>
        [HttpGet("routingnumber")]
        public IActionResult GetRoutingNumber() =>
            Ok(_financeService.GetRoutingNumber());

        /// <summary>
        /// Get Bank Identifier Code (BIC) code.
        /// </summary>
        /// <returns> Bank Identifier Code. </returns>
        [HttpGet("bic")]
        public IActionResult GetBic() =>
            Ok(_financeService.GetBic());

        /// <summary>
        /// Get an International Bank Account Number (IBAN).
        /// </summary>
        /// <returns> Bank Account Number. </returns>
        [HttpGet("iban")]
        public IActionResult GetIban() =>
            Ok(_financeService.GetIban());
    }
}