using System;
using DataGenerator.WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DataGenerator.WebApi.Controllers
{
    [Route("/api/commerce")]
    public class CommerceController : Controller
    {
        private readonly ICommerceService _commerceService;

        public CommerceController(ICommerceService commerceService)
        {
            if (commerceService == null) throw new ArgumentNullException(nameof(commerceService));
            
            _commerceService = commerceService;
        }

        /// <summary>
        /// Get a random commerce department.
        /// </summary>
        /// <returns> Commerce department. </returns>
        [HttpGet("department")]
        public IActionResult GetDepartment() =>
            Ok(_commerceService.GetDepartment());

        /// <summary>
        /// Get a random product price.
        /// </summary>
        /// <returns> Product price. </returns>
        [HttpGet("price")]
        public IActionResult GetPrice() =>
            Ok(_commerceService.GetPrice());

        /// <summary>
        /// Get random product categories.
        /// </summary>
        /// <returns> Product categories. </returns>
        [HttpGet("categories/{number}")]
        public IActionResult GetCategories(int number) =>
            Ok(_commerceService.GetCategories(number));

        /// <summary>
        /// Get a random product name.
        /// </summary>
        /// <returns> Product name. </returns>
        [HttpGet("productname")]
        public IActionResult GetProductName() =>
            Ok(_commerceService.GetProductName());

        /// <summary>
        /// Get a random color.
        /// </summary>
        /// <returns> Color. </returns>
        [HttpGet("color")]
        public IActionResult GetColor() =>
            Ok(_commerceService.GetColor());

        /// <summary>
        /// Get a random product.
        /// </summary>
        /// <returns> Product.</returns>
        [HttpGet("product")]
        public IActionResult GetProduct() =>
            Ok(_commerceService.GetProduct());

        /// <summary>
        /// Get a random product adjective.
        /// </summary>
        /// <returns> Product adjective. </returns>
        [HttpGet("productadjective")]
        public IActionResult GetProductAdjective() =>
            Ok(_commerceService.GetProductAdjective());

        /// <summary>
        /// Get a random product material.
        /// </summary>
        /// <returns> Product material.</returns>
        [HttpGet("productmaterial")]
        public IActionResult GetProductMaterial() =>
            Ok(_commerceService.GetProductMaterial());
    }
}