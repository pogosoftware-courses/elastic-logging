﻿using Bogus;
using DataGenerator.Logging.Interfaces;
using DataGenerator.Logging.Loggers;
using DataGenerator.Logging.Options;
using DataGenerator.WebApi.Middlewares;
using DataGenerator.WebApi.Services;
using DataGenerator.WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DataGenerator.WebApi
{
    public class Startup
    {
        private const string FileLoggerConfigurationSectionName = "FileLogger";
        private const string ElasticsearchLoggerConfigurationSectionName = "ElasticsearchLogger";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureLoggerOptions(services);

            RegisterLoggers(services);
            RegisterDataGenerator(services);
            RegisterServices(services);

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<LoggingMiddleware>();
            app.UseMvc();
        }

        private void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<ICommerceService, CommerceService>();
            services.AddSingleton<IFinanceService, FinanceService>();
            services.AddSingleton<INameService, NameService>();
        }

        private void RegisterDataGenerator(IServiceCollection services)
        {
            services.AddSingleton<Faker>(_ => new Faker());
        }

        private void ConfigureLoggerOptions(IServiceCollection services)
        {
            services.Configure<FileLoggerOptions>(
                Configuration.GetSection(FileLoggerConfigurationSectionName));
            services.Configure<ElasticsearchLoggerOptions>(
                Configuration.GetSection(ElasticsearchLoggerConfigurationSectionName));
        }

        private void RegisterLoggers(IServiceCollection services)
        {
            services.AddSingleton<IFileLogger, FileLogger>();
            services.AddSingleton<IElasticsearchLogger, ElasticsearchLogger>();
        }
    }
}
