using System;
using Bogus;
using DataGenerator.WebApi.Services.Interfaces;

namespace DataGenerator.WebApi.Services
{
    public class NameService : INameService
    {
        private readonly Faker _nameGenerator;

        public NameService(Faker nameGenerator)
        {
            if (nameGenerator == null) throw new ArgumentNullException(nameof(nameGenerator));

            _nameGenerator = nameGenerator;
        }

        /// <summary>
        /// Get a first name.
        /// </summary>
        /// <returns> First name. </returns>
        public string GetFirstName()
        {
            return _nameGenerator.Name.FirstName();
        }

        /// <summary>
        /// Get a last name.
        /// </summary>
        /// <returns> Last name. </returns>
        public string GetLastName()
        {
            return _nameGenerator.Name.LastName();
        }

        /// <summary>
        /// Get a full name.
        /// </summary>
        /// <returns> Full name.</returns>
        public string GetFullName()
        {
            return _nameGenerator.Name.FullName();
        }

        /// <summary>
        /// Get a random prefix for a name.
        /// </summary>
        /// <returns> Prefix. </returns>
        public string GetPrefix()
        {
            return _nameGenerator.Name.Prefix();
        }

        /// <summary>
        /// Get a random suffix for a name.
        /// </summary>
        /// <returns> Suffix. </returns>
        public string GetSuffix()
        {
            return _nameGenerator.Name.Suffix();
        }

        /// <summary>
        /// Get a random job title.
        /// </summary>
        /// <returns> Job title. </returns>
        public string GetJobTitle()
        {
            return _nameGenerator.Name.JobTitle();
        }

        /// <summary>
        /// Get a job description.
        /// </summary>
        /// <returns> Job description.</returns>
        public string GetJobDescriptor()
        {
            return _nameGenerator.Name.JobDescriptor();
        }

        /// <summary>
        /// Get a job area expertise.
        /// </summary>
        /// <returns> Job area. </returns>
        public string GetJobArea()
        {
            return _nameGenerator.Name.JobArea();
        }

        /// <summary>
        /// Get a type of job.
        /// </summary>
        /// <returns> Job type. </returns>
        public string GetJobType()
        {
            return _nameGenerator.Name.JobType();
        }
    }
}