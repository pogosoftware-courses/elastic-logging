namespace DataGenerator.WebApi.Services.Interfaces
{
    public interface INameService
    {
        /// <summary>
        /// Get a first name.
        /// </summary>
        /// <returns> First name. </returns>
        string GetFirstName();

        /// <summary>
        /// Get a last name.
        /// </summary>
        /// <returns> Last name. </returns>
        string GetLastName();

        /// <summary>
        /// Get a full name.
        /// </summary>
        /// <returns> Full name.</returns>
        string GetFullName();

        /// <summary>
        /// Get a random prefix for a name.
        /// </summary>
        /// <returns> Prefix. </returns>
        string GetPrefix();

        /// <summary>
        /// Get a random suffix for a name.
        /// </summary>
        /// <returns> Suffix. </returns>
        string GetSuffix();

        /// <summary>
        /// Get a random job title.
        /// </summary>
        /// <returns> Job title. </returns>
        string GetJobTitle();

        /// <summary>
        /// Get a job description.
        /// </summary>
        /// <returns> Job description.</returns>
        string GetJobDescriptor();

        /// <summary>
        /// Get a job area expertise.
        /// </summary>
        /// <returns> Job area. </returns>
        string GetJobArea();

        /// <summary>
        /// Get a type of job.
        /// </summary>
        /// <returns> Job type. </returns>
        string GetJobType();
    }
}