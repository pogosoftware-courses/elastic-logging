using Bogus.DataSets;

namespace DataGenerator.WebApi.Services.Interfaces
{
    public interface IFinanceService
    {
        /// <summary>
        /// Get an account number. Default length is 8 digits.
        /// </summary>
        /// <returns> Acount number. </returns>
        string GetAccount();

        /// <summary>
        /// Get an account name. Like "savings", "checking", "Home Loan" etc..
        /// </summary>
        /// <returns> Account name. </returns>
        string GetAccountName();

        /// <summary>
        /// Get a random amount. Default 0 - 1000.
        /// </summary>
        /// <returns> Amount. </returns>
        decimal GetAmount();

        /// <summary>
        /// Get a transaction type: "deposit", "withdrawal", "payment", or "invoice".
        /// </summary>
        /// <returns> Transaction type. </returns>
        string GetTransactionType();

        /// <summary>
        /// Get a random currency.
        /// </summary>
        /// <returns> Currency. </returns>
        Currency GetCurrency();

        /// <summary>
        /// Get a random credit card number with valid Luhn checksum.
        /// </summary>
        /// <returns> Credit card number. </returns>
        string GetCreditCardNumber();

        /// <summary>
        /// Get a credit card CVV.
        /// </summary>
        /// <returns> Credit card CVV. </returns>
        string GetCreditCardCvv();

        /// <summary>
        /// Get a random bitcoin address.
        /// </summary>
        /// <returns> Bitcoin address. </returns>
        string GetBitcoinAddress();

        /// <summary>
        /// Get a random ethereum address.
        /// </summary>
        /// <returns> Ethereum address. </returns>
        string GetEthereumAddress();

        /// <summary>
        /// Get an ABA routing number with valid check digit.
        /// </summary>
        /// <returns> Routing number. </returns>
        string GetRoutingNumber();

        /// <summary>
        /// Get Bank Identifier Code (BIC) code.
        /// </summary>
        /// <returns> Bank Identifier Code. </returns>
        string GetBic();

        /// <summary>
        /// Get an International Bank Account Number (IBAN).
        /// </summary>
        /// <returns> Bank Account Number. </returns>
        string GetIban();
    }
}