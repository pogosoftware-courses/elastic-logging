using System.Collections.Generic;

namespace DataGenerator.WebApi.Services.Interfaces
{
    public interface ICommerceService
    {
        /// <summary>
        /// Get a random commerce department.
        /// </summary>
        /// <returns> Commerce department. </returns>
        string GetDepartment();

        /// <summary>
        /// Get a random product price.
        /// </summary>
        /// <returns> Product price. </returns>
        string GetPrice();

        /// <summary>
        /// Get random product categories.
        /// </summary>
        /// <returns> Product categories. </returns>
        IEnumerable<string> GetCategories(int number);

        /// <summary>
        /// Get a random product name.
        /// </summary>
        /// <returns> Product name. </returns>
        string GetProductName();

        /// <summary>
        /// Get a random color.
        /// </summary>
        /// <returns> Color. </returns>
        string GetColor();

        /// <summary>
        /// Get a random product.
        /// </summary>
        /// <returns> Product.</returns>
        string GetProduct();

        /// <summary>
        /// Get a random product adjective.
        /// </summary>
        /// <returns> Product adjective. </returns>
        string GetProductAdjective();

        /// <summary>
        /// Get a random product material.
        /// </summary>
        /// <returns> Product material.</returns>
        string GetProductMaterial();
    }
}