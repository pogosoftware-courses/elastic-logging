using System;
using Bogus;
using Bogus.DataSets;
using DataGenerator.Logging.Interfaces;
using DataGenerator.WebApi.Services.Interfaces;

namespace DataGenerator.WebApi.Services
{
    public class FinanceService : IFinanceService
    {
        private readonly Faker _financeGenerator;
        private readonly IFileLogger _fileLogger;
        private readonly IElasticsearchLogger _elasticsearchLogger;

        public FinanceService(Faker financeGenerator,
            IFileLogger fileLogger,
            IElasticsearchLogger elasticsearchLogger)
        {
            if (financeGenerator == null) throw new ArgumentNullException(nameof(financeGenerator));
            if (fileLogger == null) throw new ArgumentNullException(nameof(fileLogger));
            if (elasticsearchLogger == null) throw new ArgumentNullException(nameof(elasticsearchLogger));

            _financeGenerator = financeGenerator;
            _fileLogger = fileLogger;
            _elasticsearchLogger = elasticsearchLogger;
        }

        /// <summary>
        /// Get an account number. Default length is 8 digits.
        /// </summary>
        /// <returns> Acount number. </returns>
        public string GetAccount()
        {
            return _financeGenerator.Finance.Account();
        }

        /// <summary>
        /// Get an account name. Like "savings", "checking", "Home Loan" etc..
        /// </summary>
        /// <returns> Account name. </returns>
        public string GetAccountName()
        {
            return _financeGenerator.Finance.AccountName();
        }

        /// <summary>
        /// Get a random amount. Default 0 - 1000.
        /// </summary>
        /// <returns> Amount. </returns>
        public decimal GetAmount()
        {
            return _financeGenerator.Finance.Amount();
        }

        /// <summary>
        /// Get a transaction type: "deposit", "withdrawal", "payment", or "invoice".
        /// </summary>
        /// <returns> Transaction type. </returns>
        public string GetTransactionType()
        {
            return _financeGenerator.Finance.TransactionType();
        }

        /// <summary>
        /// Get a random currency.
        /// </summary>
        /// <returns> Currency. </returns>
        public Currency GetCurrency()
        {
            return _financeGenerator.Finance.Currency();
        }

        /// <summary>
        /// Get a random credit card number with valid Luhn checksum.
        /// </summary>
        /// <returns> Credit card number. </returns>
        public string GetCreditCardNumber()
        {
            return _financeGenerator.Finance.CreditCardNumber();
        }

        /// <summary>
        /// Get a credit card CVV.
        /// </summary>
        /// <returns> Credit card CVV. </returns>
        public string GetCreditCardCvv()
        {
            return _financeGenerator.Finance.CreditCardCvv();
        }

        /// <summary>
        /// Get a random bitcoin address.
        /// </summary>
        /// <returns> Bitcoin address. </returns>
        public string GetBitcoinAddress()
        {
            return _financeGenerator.Finance.BitcoinAddress();
        }

        /// <summary>
        /// Get a random ethereum address.
        /// </summary>
        /// <returns> Ethereum address. </returns>
        public string GetEthereumAddress()
        {
            return _financeGenerator.Finance.EthereumAddress();
        }

        /// <summary>
        /// Get an ABA routing number with valid check digit.
        /// </summary>
        /// <returns> Routing number. </returns>
        public string GetRoutingNumber()
        {
            return _financeGenerator.Finance.RoutingNumber();
        }

        /// <summary>
        /// Get Bank Identifier Code (BIC) code.
        /// </summary>
        /// <returns> Bank Identifier Code. </returns>
        public string GetBic()
        {
            return _financeGenerator.Finance.Bic();
        }

        /// <summary>
        /// Get an International Bank Account Number (IBAN).
        /// </summary>
        /// <returns> Bank Account Number. </returns>
        public string GetIban()
        {
            return _financeGenerator.Finance.Iban();
        }
    }
}