using System;
using System.Collections.Generic;
using Bogus;
using DataGenerator.Logging.Interfaces;
using DataGenerator.WebApi.Services.Interfaces;

namespace DataGenerator.WebApi.Services
{
    public class CommerceService : ICommerceService
    {
        private readonly Faker _commerceGenerator;
        private readonly IFileLogger _fileLogger;
        private readonly IElasticsearchLogger _elasticsearchLogger;

        public CommerceService(Faker commerceGenerator,
            IFileLogger fileLogger,
            IElasticsearchLogger elasticsearchLogger)
        {
            if (commerceGenerator == null) throw new ArgumentNullException(nameof(commerceGenerator));
            if (fileLogger == null) throw new ArgumentNullException(nameof(fileLogger));
            if (elasticsearchLogger == null) throw new ArgumentNullException(nameof(elasticsearchLogger));

            _commerceGenerator = commerceGenerator;
            _fileLogger = fileLogger;
            _elasticsearchLogger = elasticsearchLogger;
        }

        /// <summary>
        /// Get a random commerce department.
        /// </summary>
        /// <returns> Commerce department. </returns>
        public string GetDepartment()
        {
            return _commerceGenerator.Commerce.Department();
        }

        /// <summary>
        /// Get a random product price.
        /// </summary>
        /// <returns> Product price. </returns>
        public string GetPrice()
        {
            return _commerceGenerator.Commerce.Price();
        }

        /// <summary>
        /// Get random product categories.
        /// </summary>
        /// <returns> Product categories. </returns>
        public IEnumerable<string> GetCategories(int number)
        {
            return _commerceGenerator.Commerce.Categories(number);
        }

        /// <summary>
        /// Get a random product name.
        /// </summary>
        /// <returns> Product name. </returns>
        public string GetProductName()
        {
            return _commerceGenerator.Commerce.ProductName();
        }

        /// <summary>
        /// Get a random color.
        /// </summary>
        /// <returns> Color. </returns>
        public string GetColor()
        {
            return _commerceGenerator.Commerce.Color();
        }

        /// <summary>
        /// Get a random product.
        /// </summary>
        /// <returns> Product.</returns>
        public string GetProduct()
        {
            return _commerceGenerator.Commerce.Product();
        }

        /// <summary>
        /// Get a random product adjective.
        /// </summary>
        /// <returns> Product adjective. </returns>
        public string GetProductAdjective()
        {
            return _commerceGenerator.Commerce.ProductAdjective();
        }

        /// <summary>
        /// Get a random product material.
        /// </summary>
        /// <returns> Product material.</returns>
        public string GetProductMaterial()
        {
            return _commerceGenerator.Commerce.ProductMaterial();
        }
    }
}