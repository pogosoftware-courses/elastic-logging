# Using Baets to Monitor Your Infrastructure

This is source code to course how to setup and use beats to monitor infrastructure.

## Setup Lab Environemnt
To setup you environment first ensure that you have installed VirtualBox and Vagrant. Then go to the infrastructure folder and run command:

`vagrant up` 

This bring up ubuntu 16.04 virtual machine with docker and all elastic tools.

## Setup sample dashboards

Login to the machine

`vagrant ssh`

### Dashboards for Metricbeat
Login to the metricbeat container using command:

`docker exec -it metricbeat-6.0.0 /bin/bash`

and next run command to setup dashbord:

`./metricbeat setup --dashboards`

### Dashboards for Packetbeat
Login to the packetbeat container using command:

`docker exec -it packetbeat-6.0.0 /bin/bash`

and next run command to setup dashbord:

`./packetbeat setup --dashboards`

## Kibana

To use kibana you simply paste below url to your browser:

`http://192.168.172.2:5601`